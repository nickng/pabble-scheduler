#ifndef SCHED_H__
#define SCHED_H__
#include <stddef.h>

// Scheduler for parallelism=4
void sched4(size_t idx0, size_t idx1, size_t idx2, size_t idx3, int *enable);
#endif//SCHED_H__
