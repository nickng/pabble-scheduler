# Dynamic scheduler demo

`sched.h` and `sched.c` will be generated from a pre-specified protocol, parameterised by the number of ports/datapaths available.

To build:

    mkdir build; cd build;
    cmake ..
    make

Running the examples will show the port-datapath enable matrix:

- rows = port
- cols = datapath

There should be one `1` per column and per row, if not please report with a new issue or a failing test case.