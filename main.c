#include <stdio.h>
#include <stdlib.h>
#include "sched.h"

#define TEST_CASES 6
#define DIMEN 4

int main(int argc, char *argv[])
{
    int columnIndex[TEST_CASES][4] = {
        {0 ,0, 0, 0}, /* Only 0-0 */
        {0, 1, 2, 3}, /* No conflict */
        {0, 1, 0, 1}, /* Half conflict */
        {2, 3, 0, 1}, /* No conflict (out-of-order) */
        {0, 0, 0, 3}, /* 3 conflict, 2 no conflict*/
        {1, 0, 1, 1}, /* Half conflict (not priority) */
    };
    char *cases[TEST_CASES] = {
        "Only single port enabled",
        "All pipelines parallel in order",
        "Only port 0, port 1 used",
        "All pipelines parallel not in order",
        "3-conflict + 1 unrelated",
        "Only port 0, port 1 used (not in priority order)",
    };
    int *enable = calloc(DIMEN, sizeof(int));

    for (int i=0; i<TEST_CASES; i++) {

        printf("\n%s\n", cases[i]);
        printf("Input: ");
        for (int j=0; j<4; j++) {
            printf("%d ", columnIndex[i][j]);
        }
        printf("\n");
        sched4(columnIndex[i][0], columnIndex[i][1], columnIndex[i][2], columnIndex[i][3], enable);

        for (int r=0; r<DIMEN; r++) {
            printf("Port %d:      %d\n", r, enable[r]);
        }
    }

    return 0;
}
